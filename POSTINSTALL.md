This app allows this Cloudron's users to authenticate and store their calendar
and contacts.


#### Custom permissions (e.g. shared calendar)

Per default, each user can only access their own calendar and contacts. If you
want something more complicated you can change the permissions.

You can change the permissions by pushing a file using the Cloudron CLI tool at
`/app/data/permissions`. The default content of that file is:

    [owner]
    user: .+
    collection: ^%(login)s/.*$
    permission: rw


You can extend the file using the syntax described in [the radicale documentation](http://radicale.org/rights/).
