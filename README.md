# Radicale Cloudron App

This repository contains the Cloudron app package source for [Radicale](http://radicale.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.radicale.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.radicale.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd radicale-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/) and [vdirsyncer](https://github.com/untitaker/vdirsyncer). They are creating a fresh build, install the app on your Cloudron, perform carddav tests, backup, restore and test if the carddav contacts are still ok. Due to limitations of vdirsyncer, the tests only work with valid SSL certificates.

```
cd radicale-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha test.js
```

### Installing vdirsyncer
```
virtualenv -p python3 ~/vdirsyncer_env
~/vdirsyncer_env/bin/pip install vdirsyncer
export VDIRSYNCER=~/vdirsyncer_env/bin/vdirsyncer
```

