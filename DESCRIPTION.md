### The Radicale Project is a complete CalDAV (calendar) and CardDAV (contact) server solution.

This app packages Radicale version 1.1.1

Calendars and address books can be viewed, edited and synced by calendar and contact clients on mobile phones or computers.

Officially supported clients are listed [here](http://radicale.org/user_documentation/#idcaldav-and-carddav-clients), other DAV compatible clients may work as well.

